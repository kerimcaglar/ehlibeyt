<?php


namespace App\Controller\Api;


use App\Response\ApiResponse;
use App\Service\CategoryService;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends ApiController
{
    /**
     * @Route("/categories", name="categories")
     * @param CategoryService $categoryService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(CategoryService $categoryService)
    {
        $data = $categoryService->getAll();
        return $this->createJsonResponse(ApiResponse::createSuccessResponse($data));
    }

}
