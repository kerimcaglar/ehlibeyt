<?php
/**
 * Created by PhpStorm.
 * User: kerimcaglar
 * Date: 2019-05-19
 * Time: 14:31
 */

namespace App\Controller\Api;


use App\Response\ApiResponse;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends ApiController
{
    /**
     * @Route("/users", name="users")
     * @param UserService $userService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(UserService $userService)
    {
        $data = $userService->getAll();

        return $this->createJsonResponse(ApiResponse::createSuccessResponse($data));
    }

    /**
     * @Route("/man-users", name="man-users")
     * @param UserService $userService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getMen(UserService $userService)
    {
        $data = $userService->getMen();

        return $this->createJsonResponse(ApiResponse::createSuccessResponse($data));
    }

    /**
     * @Route("/woman-users", name="woman-users")
     * @param UserService $userService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getWomen(UserService $userService)
    {
        $data = $userService->getWomen();

        return $this->createJsonResponse(ApiResponse::createSuccessResponse($data));
    }


    /**
     * @Route("/create-spouse", name="create-spouse", methods={"POST","GET"})
     * @param Request $request
     * @param UserService $userService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createSpouseAction(Request $request, UserService $userService)
    {
        $data = $userService->createSpouse($request);

        return $this->createJsonResponse(ApiResponse::createSuccessResponse($data));

    }

    /**
     * @Route("/create-user", name="create-user", methods={"POST"})
     * @param Request $request
     * @param UserService $userService
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createUser(Request $request, UserService $userService)
    {
        $data = $userService->createUser($request);
        return $this->createJsonResponse(ApiResponse::createSuccessResponse($data));
    }
}
