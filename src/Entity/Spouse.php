<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as Serializer;


/**
 * @ORM\Entity(repositoryClass="App\Repository\SpouseRepository")
 * @Serializer\ExclusionPolicy("all")
 */
class Spouse
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $husband;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $wife;

    use TimestampableEntity;
    use SoftDeleteableEntity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHusband(): ?User
    {
        return $this->husband;
    }

    public function setHusband(?User $husband): self
    {
        $this->husband = $husband;

        return $this;
    }

    public function getWife(): ?User
    {
        return $this->wife;
    }

    public function setWife(?User $wife): self
    {
        $this->wife = $wife;

        return $this;
    }
}
