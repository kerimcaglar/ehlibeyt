<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="users")
 * @Serializer\ExclusionPolicy("all")
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Serializer\Expose()
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $queue;

    /**
     * @ORM\Column(type="integer")
     */
    private $level;

    /**
     * @ORM\Column(type="integer")
     */
    private $enter_count;

    /**
     * @ORM\Column(type="string", length=11, nullable=true)
     */
    private $tc;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $password;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="category")
     * @Serializer\Expose()
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Expose()
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     */
    private $last_name;

    /**
     * @ORM\Column(type="string", length=1)
     */
    private $gender;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     * @Serializer\Expose()
     */
    private $father;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $mother;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $birth_place;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $birth_date;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $death_date;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $death_place;

    /**
     * @ORM\Column(type="string", length=60, nullable=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="smallint")
     */
    private $email_visible;

    /**
     * @ORM\Column(type="smallint")
     */
    private $phone_visible;

    /**
     * @ORM\Column(type="smallint")
     */
    private $email_status;

    /**
     * @ORM\Column(type="smallint")
     */
    private $sms_status;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $photo;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     * @ORM\JoinColumn(nullable=false)
     */
    private $updated_by;

    /**
     * @ORM\Column(type="string", length=15)
     */
    private $updated_by_ip;

    use TimestampableEntity;
    use SoftDeleteableEntity;


    public function __construct()
    {
        $this->updated_by = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQueue(): ?int
    {
        return $this->queue;
    }

    public function setQueue(int $queue): self
    {
        $this->queue = $queue;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getEnterCount(): ?int
    {
        return $this->enter_count;
    }

    public function setEnterCount(int $enter_count): self
    {
        $this->enter_count = $enter_count;

        return $this;
    }

    public function getTc(): ?string
    {
        return $this->tc;
    }

    public function setTc(?string $tc): self
    {
        $this->tc = $tc;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(?string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }

    public function getGender(): ?string
    {
        return $this->gender;
    }

    public function setGender(string $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getFather(): ?self
    {
        return $this->father;
    }

    public function setFather(?self $father): self
    {
        $this->father = $father;

        return $this;
    }

    public function getMother(): ?self
    {
        return $this->mother;
    }

    public function setMother(?self $mother): self
    {
        $this->mother = $mother;

        return $this;
    }


    public function getBirthPlace(): ?string
    {
        return $this->birth_place;
    }

    public function setBirthPlace(?string $birth_place): self
    {
        $this->birth_place = $birth_place;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birth_date;
    }

    public function setBirthDate(?\DateTimeInterface $birth_date): self
    {
        $this->birth_date = $birth_date;

        return $this;
    }

    public function getDeathDate(): ?\DateTimeInterface
    {
        return $this->death_date;
    }

    public function setDeathDate(?\DateTimeInterface $death_date): self
    {
        $this->death_date = $death_date;

        return $this;
    }

    public function getDeathPlace(): ?string
    {
        return $this->death_place;
    }

    public function setDeathPlace(?string $death_place): self
    {
        $this->death_place = $death_place;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getEmailVisible(): ?int
    {
        return $this->email_visible;
    }

    public function setEmailVisible(int $email_visible): self
    {
        $this->email_visible = $email_visible;

        return $this;
    }

    public function getPhoneVisible(): ?int
    {
        return $this->phone_visible;
    }

    public function setPhoneVisible(int $phone_visible): self
    {
        $this->phone_visible = $phone_visible;

        return $this;
    }

    public function getEmailStatus(): ?int
    {
        return $this->email_status;
    }

    public function setEmailStatus(int $email_status): self
    {
        $this->email_status = $email_status;

        return $this;
    }

    public function getSmsStatus(): ?int
    {
        return $this->sms_status;
    }

    public function setSmsStatus(int $sms_status): self
    {
        $this->sms_status = $sms_status;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->photo;
    }

    public function setPhoto(?string $photo): self
    {
        $this->photo = $photo;

        return $this;
    }


    public function getUpdatedBy(): ?self
    {
        return $this->updated_by;
    }

    public function setUpdatedBy(?self $updated_by): self
    {
        $this->updated_by = $updated_by;

        return $this;
    }

    /**
     * @return Collection|self[]
     */

    public function getUpdatedByIp(): ?string
    {
        return $this->updated_by_ip;
    }

    public function setUpdatedByIp(string $updated_by_ip): self
    {
        $this->updated_by_ip = $updated_by_ip;

        return $this;
    }
}
