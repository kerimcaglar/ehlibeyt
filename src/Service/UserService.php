<?php
/**
 * Created by PhpStorm.
 * User: kerimcaglar
 * Date: 2019-05-19
 * Time: 14:34
 */

namespace App\Service;


use App\Entity\Category;
use App\Entity\Spouse;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;

class UserService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function getAll()
    {
        $repo = $this->em->getRepository(User::class);
        return $repo->findAll();
    }

    public function getMen()
    {
        $repo = $this->em->getRepository(User::class);

        return $repo->findByGender($value = 'E');
    }

    public function getWomen()
    {
        $repo = $this->em->getRepository(User::class);

        return $repo->findByGender($value = 'K');
    }

    public function createSpouse(Request $request)
    {
        $husbandId = $request->get('husbandId');
        $wifeId = $request->get('wifeId');


        $husband = $this->em->getRepository(User::class)->findOneBy(['id'=>$husbandId]);
        $wife    = $this->em->getRepository(User::class)->findOneBy(['id'=>$wifeId]);

        $spouse = new Spouse();

        $spouse->setHusband($husband);
        $spouse->setWife($wife);
        $spouse->setCreatedAt(new \DateTime('now'));
        $spouse->setUpdatedAt(new \DateTime('now'));
        $this->em->persist($spouse);
        $this->em->flush();

    }

    public function createUser(Request $request)
    {
        $categoryId = $request->get('category');
        $fatherId = $request->get('father');
        $motherId = $request->get('mother');
        $category = $this->em->getRepository(Category::class)->findOneBy(['id' => $categoryId]);
        $father = $this->em->getRepository(User::class)->findOneBy(['id' => $fatherId]);
        $mother = $this->em->getRepository(User::class)->findOneBy(['id' => $motherId]);
        $name = $request->get('name');
        $lastName = $request->get('lastName');
        $gender = $request->get('gender');
        $password = $request->get('password');

        $user = new User();
        $user->setCategory($category);
        $user->setFather($father);
        $user->setMother($mother);
        $user->setName($name);
        $user->setLastName($lastName);
        $user->setGender($gender);
        $user->setPassword($password);
        $user->setUpdatedBy($father);


        $this->em->persist($user);
        $this->em->flush();
    }
}
