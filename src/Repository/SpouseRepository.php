<?php

namespace App\Repository;

use App\Entity\Spouse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Spouse|null find($id, $lockMode = null, $lockVersion = null)
 * @method Spouse|null findOneBy(array $criteria, array $orderBy = null)
 * @method Spouse[]    findAll()
 * @method Spouse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpouseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Spouse::class);
    }

    // /**
    //  * @return Spouse[] Returns an array of Spouse objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Spouse
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
