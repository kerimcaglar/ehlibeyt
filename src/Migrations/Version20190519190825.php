<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190519190825 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE categories (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(30) NOT NULL, description VARCHAR(120) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE users (id INT AUTO_INCREMENT NOT NULL, category_id INT DEFAULT NULL, father_id INT NOT NULL, mother_id INT DEFAULT NULL, updated_by_id INT NOT NULL, queue INT NOT NULL, level INT NOT NULL, enter_count INT NOT NULL, tc VARCHAR(11) DEFAULT NULL, password VARCHAR(32) NOT NULL, name VARCHAR(255) NOT NULL, last_name VARCHAR(255) DEFAULT NULL, gender VARCHAR(1) NOT NULL, birth_place VARCHAR(255) DEFAULT NULL, birth_date DATETIME DEFAULT NULL, death_date DATETIME DEFAULT NULL, death_place VARCHAR(255) DEFAULT NULL, email VARCHAR(60) DEFAULT NULL, phone VARCHAR(25) DEFAULT NULL, email_visible SMALLINT NOT NULL, phone_visible SMALLINT NOT NULL, email_status SMALLINT NOT NULL, sms_status SMALLINT NOT NULL, photo VARCHAR(50) DEFAULT NULL, updated_by_ip VARCHAR(15) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_1483A5E912469DE2 (category_id), INDEX IDX_1483A5E92055B9A2 (father_id), INDEX IDX_1483A5E9B78A354D (mother_id), INDEX IDX_1483A5E9896DBBDE (updated_by_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E912469DE2 FOREIGN KEY (category_id) REFERENCES categories (id)');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E92055B9A2 FOREIGN KEY (father_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E9B78A354D FOREIGN KEY (mother_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE users ADD CONSTRAINT FK_1483A5E9896DBBDE FOREIGN KEY (updated_by_id) REFERENCES users (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E912469DE2');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E92055B9A2');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E9B78A354D');
        $this->addSql('ALTER TABLE users DROP FOREIGN KEY FK_1483A5E9896DBBDE');
        $this->addSql('DROP TABLE categories');
        $this->addSql('DROP TABLE users');
    }
}
