<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190602092759 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE spouse (id INT AUTO_INCREMENT NOT NULL, husband_id INT DEFAULT NULL, wife_id INT DEFAULT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, deleted_at DATETIME DEFAULT NULL, INDEX IDX_CD13CC0F6D960872 (husband_id), INDEX IDX_CD13CC0F18D2F6B7 (wife_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE spouse ADD CONSTRAINT FK_CD13CC0F6D960872 FOREIGN KEY (husband_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE spouse ADD CONSTRAINT FK_CD13CC0F18D2F6B7 FOREIGN KEY (wife_id) REFERENCES users (id)');
        $this->addSql('ALTER TABLE users CHANGE father_id father_id INT NOT NULL, CHANGE updated_by_id updated_by_id INT NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE spouse');
        $this->addSql('ALTER TABLE users CHANGE father_id father_id INT DEFAULT NULL, CHANGE updated_by_id updated_by_id INT DEFAULT NULL');
    }
}
